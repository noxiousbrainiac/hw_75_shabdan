import React from 'react';
import AppContainer from "./containers/AppContainer/AppContainer";

const App = () => {
    return (
        <>
            <AppContainer/>
        </>
    );
};

export default App;