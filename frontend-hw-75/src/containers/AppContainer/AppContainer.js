import React from 'react';
import {Container, IconButton, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {decodePost, encodePost, onchangeInput} from "../../store/actions";
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';


const AppContainer = () => {
    const dispatch = useDispatch();
    const {encodedMessage, decodedMessage, password} = useSelector(state => ({
        encodedMessage: state.encodedMessage,
        password: state.password,
        decodedMessage: state.decodedMessage
    }));

    const decodeClick = () => {
        const obj = {
            password: password,
            message: decodedMessage
        }
        if (password.length !== 0 && decodedMessage.length !== 0) {
            dispatch(decodePost(obj));
        }
    }

    const encodeClick = () => {
        const obj = {
            password: password,
            message: encodedMessage
        }
        if (password.length !== 0 && encodedMessage.length !== 0) {
            dispatch(encodePost(obj));
        }
    }

    return (
        <Container>
            <div style={{display: "flex", flexDirection: "column", width: "400px"}}>
                <TextField
                    style={{marginBottom: "10px"}}
                    label="Decoded message"
                    name="decodedMessage"
                    onChange={e => dispatch(onchangeInput(e))}
                    value={decodedMessage}
                />
                <div style={{display: "flex"}}>
                    <TextField
                        style={{marginBottom: "10px", flexGrow: "1"}}
                        label="Password"
                        type="password"
                        name="password"
                        onChange={e => dispatch(onchangeInput(e))}
                        value={password}
                    />
                    <IconButton onClick={encodeClick}>
                        <ArrowUpwardIcon/>
                    </IconButton>
                    <IconButton onClick={decodeClick}>
                        <ArrowDownwardIcon/>
                    </IconButton>
                </div>
                <TextField
                    style={{marginBottom: "10px"}}
                    label="Encoded message"
                    name="encodedMessage"
                    onChange={e => dispatch(onchangeInput(e))}
                    value={encodedMessage}
                />
            </div>
        </Container>
    );
};

export default AppContainer;