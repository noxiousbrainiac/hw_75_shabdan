import axios from "axios";

export const DECODE = "DECODE";
export const ENCODE = "ENCODE";
export const ONCHANGE_INPUT = "ONCHANGE_INPUT";

export const decodeMessage = (message) => ({type: DECODE, payload: message});
export const encodeMessage = (message) => ({type: ENCODE, payload: message});

export const onchangeInput = e => ({type: ONCHANGE_INPUT, name: e.target.name, value: e.target.value});

export const decodePost = (message) => async (dispatch) => {
    try {
        const {data} = await axios.post('http://localhost:8000/crypt/decode', message);
        dispatch(decodeMessage(data.decoded));
    } catch (e) {
        console.log(e);
    }
}

export const encodePost = (message) => async (dispatch) => {
    try {
        const {data} = await axios.post('http://localhost:8000/crypt/encode', message);
        dispatch(encodeMessage(data.encoded));
    } catch (e) {
        console.log(e);
    }
}