import {DECODE, ENCODE, ONCHANGE_INPUT} from "./actions";

const initialState = {
    decodedMessage: "",
    encodedMessage: "",
    password: ""
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DECODE:
            return {...state, encodedMessage: action.payload, decodedMessage: ""};
        case ENCODE:
            return {...state, decodedMessage: action.payload, encodedMessage: ""};
        case ONCHANGE_INPUT:
            return {...state, [action.name]: action.value};
        default:
            return state;
    }
}

export default reducer;