const express = require('express');
const app = express();
const cors = require('cors');
const routes = require('./app/Routes');
const port = 8000;

app.use(express.json());
app.use(cors());
app.use('/crypt', routes);

app.listen(port , () => {
    console.log('server on port:' , port);
})

